# Skill-Riddles

A box without hinges, key, or lid, yet golden treasure inside is hid. What is it?

<br>

This is an example skill you can use to build your own skills.

[![pipeline status](https://gitlab.com/Jaco-Assistant/Skill-Riddles/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/Skill-Riddles/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/Skill-Riddles/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/Skill-Riddles/-/commits/master)

<br>

There are **some rules** a skill has to follow that it works correctly:

- If it needs custom speech commands add them into `dialog/nlu/{language}/nlu.md`. \
  See this skills's `nlu.md` file for instructions how to write it. \
  Use intents from the [Dialogs-Skill](https://gitlab.com/Jaco-Assistant/Skill-Dialogs) if possible. \
  You also should add custom speech responses to `dialog/talks/{language}.json`
- Create a `config.template.yaml` file and add the permissions your skill needs. \
  The `config["user"]` can be adjusted by the users, who then can save the file as `config.yaml` \
  The `config["system"]` is in the same file, so that users can easily see the used permissions and topics.
- If you want to run some code add a python file starting with `action-` or `action_`. \
  This will be the start script of your skill and called without any parameters. \
  You can use extra software by adding a `Containerfile_amd64` and/or `Containerfile_arm64` to build a custom container image.
- If the skill needs to save persistent data, write to the `skilldata/` directory. \
  Else the data will be lost upon container restart.
- If you want to publish the skill in the skillstore also add the `skillstore.json` file.

<br>

So an example **skill structure** would look like this:

```
YourSkillName
    dialog
        nlu
            languagekeyA
                nlu.md
                entityB.txt
        talks
            languagekeyA.json
    skilldata
        .gitignore (ignore everthing)
    .gitignore (config.yaml, ...)
    action-runsmyskill.py
    config.template.yaml
    Containerfile_arm64
    Containerfile_amd64
    skillstore.json
```

<br>
  
Some **other notes**:
- All the skills files will be mounted read-only into the container at `/Jaco-Master/skills/skills/{Your-Skill-Name}/`. \
    Only the `skilldata/` directory will be mounted with write permissions.
- You are free to install whatever you like in the container.
- The `assistant.py` file you can import from `jacolib` contains a lot of helpful functions you will need. 
    Look at it here [Jaco-Master](https://gitlab.com/Jaco-Assistant/jacolib/-/blob/master/jacolib/assistant.py).
- Currently some permissions are not enforced, but this will change in later releases.
- The nlu module will automatically replace letters in your sample sentences which the stt module does not recognize.
- Add a license that others can use parts of your skill for creating their own skill.
- The keys to the different mqtt topics the skill uses will be copied into the `skilldata/` folder. \
    All topic payloads are encrypted, you can't read or write to them without the keys.
- Please write your readme and all code related texts in english, that everyone can understand it easily 
    (this also makes it easier to translate your skill to other languages).

<br>

What **you can learn** in this skill:

- Basic skill setup
- Write to persistent storage
- Check user inputs

**Complexity**: Easy

<br>

Run skill solely for debugging purposes: \
(Assumes mqtt-broker already running)

```bash
docker run --network host --rm \
  --volume `pwd`/skills/skills/Skill-Riddles/:/Jaco-Master/skills/skills/Skill-Riddles/:ro \
  --volume `pwd`/skills/skills/Skill-Riddles/skilldata/:/Jaco-Master/skills/skills/Skill-Riddles/skilldata/ \
  --volume `pwd`/../jacolib/:/Jaco-Master/jacolib/:ro \
  --volume `pwd`/userdata/config/:/Jaco-Master/userdata/config/:ro \
  -it master_base_image_amd64 python3 /Jaco-Master/skills/skills/Skill-Riddles/action-riddle.py
```

Run tests: \
(Check out [gitlab-ci](.gitlab-ci.yml#L17) for the steps)

```bash
docker build -t testing_jaco_mtr - < ../Jaco-Master/tests/Containerfile

docker run --network host --rm \
  --volume `pwd`/skills/skills/Skill-Riddles/:/Jaco-Master/skills/skills/Skill-Riddles/ \
  -it testing_jaco_mtr
```

### Riddle Sources

**English**

- Translations from German
- http://brainden.com/logic-riddles.htm
- https://www.rd.com/funny-stuff/challenging-riddles/
- https://www.riddles.com/best-riddles

**German**

- https://www.thoughtco.com/german-riddles-1444309
- https://www.germanlw.com/riddles/
- https://www.raetselstunde.de/text-raetsel/vers-raetsel/vers-raetsel-001.html
- https://www.janko.at/Raetsel/Gedichte/

**French**

- Translations from English
- https://riaumont.net/fr/scouting/labo-scout/boite-a-idees-scoutes/les-enigmes-de-frere-gris/

**Spanish**

- Translations from English
