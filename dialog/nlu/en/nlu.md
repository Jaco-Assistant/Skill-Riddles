`This is where you define your skills intents and entities.` \
`You can make comments with the `` markup signs. ` 

## intent:get_riddle
`This is a simple intent without entities.` \
`It's recommended to use snake_case_style for the name.` \
`It will later be prefixed with your skill name.` \
`Only use a minus "-" sign for the list, no asterisk or plus.`
- I want a riddle
- Let me guess something
- I need something to think about
- I'd like a new puzzle
- `Alternatives or optional words can be defined like this:` 
- New riddle (please|right now| )
- Give me a riddle
- A new riddle please
- Give me a riddle
- Tell me a new riddle

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`The file's name has to be the same as the lookup name.` \
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated, this might take some time.`
- The answer is [snow](riddle_answers.txt)
- The solution is [doll](riddle_answers.txt)
- The solution is a [tree](riddle_answers.txt)
- The answer (of|to) the riddle is [fire](riddle_answers.txt)
- Answer is [rainbow](riddle_answers.txt)
- The solution of the riddle is a [car](riddle_answers.txt)
- For the open riddle the correct solution is [coffin](riddle_answers.txt)

`You can find further options, for example how to use multiple entities of the same type or entities from other skills in the ` [Test-Skill](https://gitlab.com/Jaco-Assistant/Skill-Tests)`.`
