`This is where you define your skills intents and entities.` \
`You can make comments with the `` markup signs. ` 

## intent:get_riddle
`This is a simple intent without entities.` \
`It's recommended to use snake_case_style for the name.` \
`It will later be prefixed with your skill name.` \
`Only use a minus "-" sign for the list, no asterisk or plus.`
- Ich möchte ein Rätsel
- Lass mich was rätseln
- Ich brauch was zum Nachdenken
- Ich hätte gerne ein neues Rätsel
- `Alternatives or optional words can be defined like this:` 
- Ein neues Rätsel (bitte|jetzt sofort| )
- Erzähle mir ein Rätsel
- Gib mir ein (Rätsel|Riddle)
- Neues (Rätsel|Riddle)

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`The file's name has to be the same as the lookup name.` \
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated, this might take some time.`
- Die Antwort ist [Schnee](riddle_answers.txt)
- Die Lösung ist [Puppe](riddle_answers.txt)
- Die Antwort auf das Rätsel ist [Feuer](riddle_answers.txt)
- Des Rätsels Lösung ist [Baum](riddle_answers.txt)
- Die (Antwort|Lösung) des Rätsels ist [Brot](riddle_answers.txt)
- Antwort ist [Regenbogen](riddle_answers.txt)
- Für das offene Rätsel ist die korrekte Antwort [Sarg](riddle_answers.txt)

`You can find further options, for example how to use multiple entities of the same type or entities from other skills in the ` [Test-Skill](https://gitlab.com/Jaco-Assistant/Skill-Tests)`.`
