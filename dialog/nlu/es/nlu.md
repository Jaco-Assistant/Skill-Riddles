`This is where you define your skills intents and entities.` \
`You can make comments with the `` markup signs. ` 

## intent:get_riddle
`This is a simple intent without entities.` \
`It's recommended to use snake_case_style for the name.` \
`It will later be prefixed with your skill name.` \
`Only use a minus "-" sign for the list, no asterisk or plus.`
- Quiero un acertijo
- Déjame adivinar algo
- Necesito algo en que pensar
- Me gustaría un nuevo rompecabezas
- `Alternatives or optional words can be defined like this:` 
- Nuevo Acertijo (por favor|ahora mismo| )
- Un nuevo acertijo, por favor.
- Dame un acertijo
- Dime un nuevo acertijo

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`The file's name has to be the same as the lookup name.` \
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated, this might take some time.`
- La respuesta es [nieve](riddle_answers.txt)
- La solución es [muñeca](riddle_answers.txt)
- La respuesta al acertijo es [fuego](riddle_answers.txt)
- La respuesta es (un|el) [árbol](riddle_answers.txt)
- La respuesta del enigma es [pan](riddle_answers.txt)
- La respuesta al enigma es un [coche](riddle_answers.txt)
- Para el enigma abierto la respuesta correcta es [ataúd](riddle_answers.txt)

`You can find further options, for example how to use multiple entities of the same type or entities from other skills in the ` [Test-Skill](https://gitlab.com/Jaco-Assistant/Skill-Tests)`.`
