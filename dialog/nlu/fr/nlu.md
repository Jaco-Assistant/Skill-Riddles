`This is where you define your skills intents and entities.` \
`You can make comments with the `` markup signs. ` 

## intent:get_riddle
`This is a simple intent without entities.` \
`It's recommended to use snake_case_style for the name.` \
`It will later be prefixed with your skill name.` \
`Only use a minus "-" sign for the list, no asterisk or plus.`
- Je veux une énigme
- Donne-moi quelque chose à deviner
- J'ai besoin de réfléchir à quelque chose
- J'aimerais avoir une nouvelle énigme
- `Alternatives or optional words can be defined like this:` 
- Nouvelle énigme (s'il vous plaît|en ce moment| )
- Donne-moi une énigme
- Un nouvelle énigme, s'il te plaît
- Soumets-moi une énigme

## lookup:riddle_answers
`If you want to use entities, create a lookup file.` \
`The file's name has to be the same as the lookup name.` \
riddle_answers.txt

## intent:check_riddle
`This is an intent with entities.` \
`Every entity example used here also has to be in the lookup file or it will be left out.` \
`Be careful if you use more than one entity in a sentence, for the language model every possible combination is generated, this might take some time.`
- La réponse est la [neige](riddle_answers.txt)
- La solution est la [poupée](riddle_answers.txt)
- La (réponse à|solution de) l'énigme est le [feu](riddle_answers.txt)
- la réponse est [arc-en-ciel](riddle_answers.txt)
- La solution de l'énigme est [étincelle](riddle_answers.txt)
- Pour l'énigme, la bonne réponse est [cercueil](riddle_answers.txt)
- La réponse est les [ciseaux](riddle_answers.txt)
- La réponse est l'[arbre](riddle_answers.txt)

`You can find further options, for example how to use multiple entities of the same type or entities from other skills in the ` [Test-Skill](https://gitlab.com/Jaco-Assistant/Skill-Tests)`.`
