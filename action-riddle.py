import os
import random
import sys

from jacolib import assistant, utils

sys.path.append("/Jaco-Master/skills/")
import text_tools  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
assist: assistant.Assistant
riddles: list


# ==================================================================================================


def load_riddles(lang):
    """Read all riddles from file and shuffle them"""

    path = filepath + "riddles/" + lang.lower() + ".json"
    rids = utils.load_json_file(path)
    random.shuffle(rids)
    return rids


# ==================================================================================================


def get_new_riddle():
    """Get first entry and append it to the end -> no repetition until every entry selected"""

    riddle = riddles.pop(0)
    riddles.append(riddle)
    write_current_riddle(riddle["id"])
    return riddle


# ==================================================================================================


def write_current_riddle(riddle_id):
    """Write riddle id in status file"""

    path = filepath + "skilldata/status.txt"
    with open(path, "w+", encoding="utf-8") as file:
        file.write(str(riddle_id))


# ==================================================================================================


def check_current_riddle():
    """Get current riddle id from status file if it already exists"""

    path = filepath + "skilldata/status.txt"
    if os.path.isfile(path):
        with open(path, "r", encoding="utf-8") as file:
            line = next(file)
            rid = int(line)
    else:
        rid = 0
    return rid


# ==================================================================================================


def callback_get_riddle(message):
    """Read new riddle or repeat open riddle"""

    riddle_id = check_current_riddle()

    if riddle_id == 0:
        riddle = get_new_riddle()
        result_sentence = riddle["question"]
    else:
        riddle = [r for r in riddles if r["id"] == riddle_id][0]
        result_sentence = riddle["question"]

    assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def callback_check_riddle(message):
    """Check if the solution of the user is correct, then ask if the user wants to hear a new
    riddle if the solution was right"""

    riddle_id = check_current_riddle()
    solution_correct = False

    if riddle_id == 0:
        # User has no open riddle
        result_sentence = assist.get_random_talk("no_riddle")
    else:
        # Check if the solution was correct
        solutions = assist.extract_entities(message, "skill_riddles-riddle_answers")
        if len(solutions) == 1:
            riddle = [r for r in riddles if r["id"] == riddle_id][0]
            solution = solutions[0]
            lang = assist.get_global_config()["language"]
            correct_answer = text_tools.clean_text_for_stt(riddle["answer"], lang)

            if solution == correct_answer:
                write_current_riddle(0)
                solution_correct = True
            else:
                result_sentence = assist.get_random_talk("wrong_solution")
                result_sentence = result_sentence.format(solution)
        else:
            result_sentence = assist.get_random_talk("not_understood")

    if not solution_correct:
        assist.publish_answer(result_sentence, message["satellite"])
    else:
        # For the question intents always use the full intent name: skill_name-intent_name
        qi = ["skill_dialogs-confirm", "skill_dialogs-refuse"]
        question_sentence = assist.get_random_talk("new_riddle_question")

        # Only send answer or question not both together
        text = assist.get_random_talk("correct_solution") + " " + question_sentence
        answer = assist.publish_question(
            text, question_intents=qi, satellite=message["satellite"]
        )

        if answer != {}:
            if answer["intent"]["name"] == "skill_dialogs-confirm":
                callback_get_riddle(message)
            else:
                result_sentence = assist.get_random_talk("new_riddle_refused")
                assist.publish_answer(result_sentence, message["satellite"])


# ==================================================================================================


def main():
    global assist, riddles

    assist = assistant.Assistant(repo_path=filepath)
    riddles = load_riddles(assist.get_global_config()["language"])

    # If you use your own skill's intents you can use the short name from your nlu file
    # Else you will have to use the complete topic name, like 'Jaco/Intents/SkillName/IntentName'
    assist.add_topic_callback("get_riddle", callback_get_riddle)
    assist.add_topic_callback("check_riddle", callback_check_riddle)
    assist.run()


# ==================================================================================================

if __name__ == "__main__":
    main()
